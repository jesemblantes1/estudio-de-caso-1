class Empleado 
{
    String primerNombre;
    String apellidoPaterno;
    String numeroSeguroSocial;
    // constructor con 3 argumentos
    public Empleado(String primerNombre, String apellidoPaterno, String numeroSeguroSocial)
    {
        this.primerNombre = primerNombre;
        this.apellidoPaterno = apellidoPaterno;
        this.numeroSeguroSocial = numeroSeguroSocial;
    }
    
    public String obtenerPrimerNombre() 
    {
        return primerNombre;
    }

    public String obtenerApellidoPaterno()
    {
        return apellidoPaterno;
    }
    
    public String obtenerNumeroSeguroSocial() 
    {
        return numeroSeguroSocial;
    }
    
    public final void setPrimerNombre(String primerNombre) 
    {
        this.primerNombre = primerNombre;
    }

    public final void setApellidoPaterno(String apellidoPaterno) 
    {
        this.apellidoPaterno = apellidoPaterno;
    }
    
    public final void setNumeroSeguroSocial(String numeroSeguroSocial)
    {
        this.numeroSeguroSocial = numeroSeguroSocial;
    }

    @Override
    public String toString()
    {
        return String.format("%s: %s%n%s: %s%n%s: %s%n", "Primer Nombre", obtenerPrimerNombre(), "Apellido Paterno", obtenerApellidoPaterno(), "Numero De Seguro Social", obtenerNumeroSeguroSocial());
    }
}





class EmpleadoPorComision extends Empleado //extiende los atributos de la superclase a la subclase
{
            private double ventasBrutas; // ventas totales por semana
            private double tarifaComision; // porcentaje de comisión  
            // constructor con cinco argumentos
    public EmpleadoPorComision(String primerNombre, String apellidoPaterno,String numeroSeguroSocial, double ventasBrutas,double tarifaComision)
    {
        super( primerNombre, apellidoPaterno, numeroSeguroSocial );//invoca a el constructor de la superclase desde en constructor de la subclase.
        // la llamada implícita al constructor de Object ocurre aquí
        // si ventasBrutas no son válidas, lanza excepción
        if (ventasBrutas < 0.0)throw new IllegalArgumentException("Ventas brutas debe ser >= 0.0");
        // si tarifaComision no es válida, lanza excepción
        if (tarifaComision >= 1.0 || tarifaComision <= 0.0)throw new IllegalArgumentException("La tarifa de comision debe ser > 0.0 y < 1.0");
        this.primerNombre = primerNombre;
        this.apellidoPaterno = apellidoPaterno;
        this.numeroSeguroSocial = numeroSeguroSocial;
        this.ventasBrutas = ventasBrutas;
        this.tarifaComision = tarifaComision;
    } // fin del constructor
      // establece el monto de ventas brutas
    public void establecerVentasBrutas(double ventasBrutas)
    {
        if (ventasBrutas < 0.0) throw new IllegalArgumentException("Las ventas brutas deben ser >= 0.0");
        this.ventasBrutas = ventasBrutas;
    }
    // devuelve el monto de ventas brutas
    public double obtenerVentasBrutas()
    {
        return ventasBrutas;
    }
    // establece la tarifa de comisión
    public void establecerTarifaComision(double tarifaComision)
    {
        if (tarifaComision <= 0.0 || tarifaComision >= 1.0)throw new IllegalArgumentException("La tarifa de comisión debe ser > 0.0 y < 1.0");
        this.tarifaComision = tarifaComision;
    }
    // devuelve la tarifa de comisión
    public double obtenerTarifaComision()
    {
        return tarifaComision;
    }
     // calcula los ingresos
    public double ingresos()
    {
        return obtenerTarifaComision() * obtenerVentasBrutas();
    }
    // devuelve representación String del objeto EmpleadoPorComision
    @Override
    public String toString()
    {
        return String.format("%s: %s %s%n%s: %s%n%s: %.2f%n%s: %.2f","empleado por comision", obtenerPrimerNombre(), obtenerApellidoPaterno(),"numero de seguro social", obtenerNumeroSeguroSocial(),"ventas brutas", obtenerVentasBrutas(),"tarifa de comision", obtenerTarifaComision());
    }
    } // fin de la clase EmpleadoPorComision




class EmpleadoBaseMasComision extends EmpleadoPorComision//extiende los atributos de la superclase a la subclase
{
    private double salarioBase; // salario base por semana
    // constructor
        public EmpleadoBaseMasComision(String primerNombre, String apellidoPaterno, String numeroSeguroSocial, double ventasBrutas, double tarifaComision, double salarioBase)
    {
         super(primerNombre, apellidoPaterno, numeroSeguroSocial,ventasBrutas, tarifaComision);//invoca a el constructor de la superclase desde en constructor de la subclase.
         if (salarioBase < 0.0) throw new IllegalArgumentException("El salario base debe ser >= 0.0");
         this.salarioBase = salarioBase;
    }
    
    // establece el salario base
        public void establecerSalarioBase(double salarioBase)
    {
         if (salarioBase < 0.0) throw new IllegalArgumentException("El salario base debe ser >= 0.0");
        
        this.salarioBase = salarioBase;
    }
    
     // devuelve el salario base
         public double obtenerSalarioBase()
    {
         return salarioBase;
    }
    
     // calcula los ingresos; sobrescribe el método ingresos en EmpleadoPorComision
    @Override
    public double ingresos()
    {
     return obtenerSalarioBase() + super.ingresos();
    }
    
    // devuelve representación String de un objeto EmpleadoBaseMasComision
    @Override
     public String toString()
    {
        return String.format("%s %s; %s: $%,.2f","con salario base", super.toString(),"salario base", obtenerSalarioBase());
    }
} // fin de la clase EmpleadoBaseMasComision




public class PruebaEmpleadoPorComision
{
    public static void main(String[] args)
    {
        // crea instancia de objeto EmpleadoPorComision
        EmpleadoPorComision empleado = new EmpleadoPorComision("Sue", "Jones", "222-22-2222", 10000, .06);
        // obtiene datos del empleado por comisión
        System.out.println("Informacion del empleado obtenida por los metodos establecer:");
        System.out.printf("%n%s %s%n", "El primer nombre es",
        empleado.obtenerPrimerNombre());
        System.out.printf("%s %s%n", "El apellido paterno es",
        empleado.obtenerApellidoPaterno());
        System.out.printf("%s %s%n", "El numero de seguro social es",
        empleado.obtenerNumeroSeguroSocial());
        System.out.printf("%s %.2f%n", "Las ventas brutas son", empleado.obtenerVentasBrutas());
        System.out.printf("%s %.2f%n", "La tarifa de comision es",
        empleado.obtenerTarifaComision());
        empleado.establecerVentasBrutas(500);
        empleado.establecerTarifaComision(.1);
        System.out.printf("%n%s:%n%n%s%n","Informacion actualizada del empleado, obtenida mediante toString",empleado);
    } // fin de main
} // fin de la clase PruebaEmpleadoPorComision



class PruebaEmpleadoBaseMasComision
{
        public static void main(String[] args)
        {
             // crea instancia de objeto EmpleadoBaseMasComision
            EmpleadoBaseMasComision empleado = new EmpleadoBaseMasComision("Bob", "Lewis", "333-33-3333", 5000, .04, 300);
            // obtiene datos del empleado por comisión con sueldo base
            System.out.println("Informacion del empleado obtenida por metodos establecer: %n");
            System.out.printf("%s %s%n", "El primer nombre es", empleado.obtenerPrimerNombre());
            System.out.printf("%s %s%n", "El apellido es", empleado.obtenerApellidoPaterno());
            System.out.printf("%s %s%n", "El numero de seguro social es", empleado.obtenerNumeroSeguroSocial());
            System.out.printf("%s %.2f%n", "Las ventas brutas son", empleado.obtenerVentasBrutas());
            System.out.printf("%s %.2f%n", "La tarifa de comision es", empleado.obtenerTarifaComision());
            System.out.printf("%s %.2f%n", "El salario base es",empleado.obtenerSalarioBase());
            empleado.establecerSalarioBase(1000);
            System.out.printf("%n%s:%n%n%s%n","Informacion actualizada del empleado, obtenida por toString",empleado.toString());
        } // fin de main
} // fin de la clase PruebaEmpleadoBaseMasComision
